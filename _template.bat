@ECHO off
REM *******************************************************
REM * @ Teraterm によるSSHログイン自動化バッチファイルのひな型
REM * @ Rev. 2.4.4
REM * @ author: y.ohta
REM *******************************************************

SETLOCAL enabledelayedexpansion



REM //////// 環境ごとの編集箇所ここから ////////

REM (↓↓ 接続情報定義 ↓↓)
REM （パスワードとユーザ名にて、"="と"&"は"^"を直前につけてエスケープする必要がある（例："a&A=b" → "a^&A^=b"））
REM （"%"のエスケープは"%%"）
SET Host=[必須]
SET Port=22
SET User="[必須]"
SET Password="[選択必須]"
SET KeyFilePath=[選択必須]
REM (↑↑ 接続情報定義 ↑↑)

REM (↓↓ 環境設定 ↓↓)
REM SET LogFileDirPath=%~dp0log\hoge[任意]
REM SET WindowTitle=[任意]
REM (↑↑ 環境設定 ↑↑)

REM (↓↓ キーマップ設定ファイル読み込み ↓↓)
REM ※ NUMキーを使えるようにする
SET KeyCnfFilePath=%~dp0.ini\MYKEYBOARD.CNF
REM (↑↑ キーマップ設定ファイル読み込み ↑↑)

REM (↓↓ （背景色など）環境設定ファイル読み込み ↓↓)
REM ※ 背景色を変更したい場合はREMコメントを外す
REM SET IniFilePath=%~dp0.ini\tt_env_honban.ini
REM (↑↑ （背景色など）環境設定ファイル読み込み ↑↑)

REM //////// 環境ごとの編集箇所ここまで ////////



REM //////// 以下、共通の処理 ////////

REM 外部設定ファイルのパス定義
SET ConfFile=%~dp0.ini\common.ini
REM 外部設定ファイルの読み込み
IF NOT EXIST %ConfFile% (
  ECHO [error] - "%ConfFile%" is not found
  PAUSE
) ELSE (
  REM 外部設定ファイルから変数の取り出し
  FOR /f "tokens=1,* delims==" %%a IN (%ConfFile%) DO (
    SET %%a=%%b
  )
  
  IF NOT EXIST !TeratermPath! (
    REM iniファイルに記載されたTeratermのパスが見つからない場合のエラー処理
    ECHO [error] - "!TeratermPath!" is not found [in "%ConfFile%"]
    PAUSE
  ) ELSE (
    REM 接続処理
    SET Isset=TRUE
    IF ""%Host%""=="""" SET Isset=FALSE
    IF "%Host%"=="""" SET Isset=FALSE
    IF ""%Port%""=="""" SET Isset=FALSE
    IF "%Port%"=="""" SET Isset=FALSE
    IF ""%User%""=="""" SET Isset=FALSE
    IF "%User%"=="""" SET Isset=FALSE
    
    IF !Isset! == FALSE (
      REM 接続情報不足エラー出力
      ECHO [error] - connection setting is invalid
      PAUSE
    ) ELSE (
      REM オプションとして接続情報を入力
      SET Option=%Host%:%Port% /user=^%User%
      
      REM パスワード認証の場合、オプションを追加
      SET Isset=TRUE
      IF ""%Password%""=="""" SET Isset=FALSE
      IF "%Password%"=="""" SET Isset=FALSE
      IF !Isset! == TRUE (
         SET Option=!Option! /passwd=^%Password%
      )
      
      REM 鍵認証の場合、オプションを追加
      SET Isset=TRUE
      IF ""%KeyFilePath%""=="""" SET Isset=FALSE
      IF "%KeyFilePath%"=="""" SET Isset=FALSE
      IF !Isset! == TRUE (
        SET Option=!Option! /auth=publickey /keyfile=%KeyFilePath%
      )
      
      REM 環境設定変数でログ保存の指定がある場合、オプションを追加
      SET Isset=TRUE
      IF ""%LogFileDirPath%""=="""" SET Isset=FALSE
      IF "%LogFileDirPath%"=="""" SET Isset=FALSE
      IF !Isset! == TRUE (
        REM ログファイルディレクトリの作成
        SET IsDir=FALSE
        IF EXIST "%LogFileDirPath%" SET IsDir=TRUE
        IF EXIST "%LogFileDirPath%\" SET IsDir=TRUE
        IF !IsDir! == FALSE (
          REM 意思確認
          ECHO ログディレクトリ変数（LogFileDirPath）: "!LogFileDirPath!"
          SET /P FLG_MKDIR="指定されたログディレクトリが存在しません。作成しますか？(y/n):" %FLG_MKDIR%
          IF "!FLG_MKDIR!" == "y" (
            MKDIR %LogFileDirPath%\
          ) ELSE (
            ECHO ログディレクトリを作成せずに処理終了しました。バッチファイル内のログディレクトリ変数（LogFileDirPath）の設定を見直してください。
            PAUSE
            EXIT
          )
        )
        
        REM ログファイル名に使用する現在日時の取得
        SET yy=%date:~2,2%
        SET mm=%date:~5,2%
        SET dd=%date:~8,2%
        SET time2=%time: =0%
        SET hh=!time2:~0,2!
        SET mn=!time2:~3,2!
        SET ss=!time2:~6,2!
        
        REM ログファイルのパスを取得
        SET LogFilePath=%LogFileDirPath%\ttlog_!yy!!mm!!dd!_!hh!!mn!!ss!.log
        
        REM ログファイルのパスをオプション指定
        SET Option=!Option! /l=!LogFilePath!
      )
      
      REM 環境設定変数でウィンドウタイトルの指定がある場合、オプションを追加
      SET Isset=TRUE
      IF ""%WindowTitle%""=="""" SET Isset=FALSE
      IF "%WindowTitle%"=="""" SET Isset=FALSE
      IF !Isset! == TRUE (
        SET Option=!Option! /w=%WindowTitle%
      )
      
      REM キーマップ設定ファイル指定がある場合、オプションを追加
      SET Isset=TRUE
      IF ""%KeyCnfFilePath%""=="""" SET Isset=FALSE
      IF "%KeyCnfFilePath%"=="""" SET Isset=FALSE
      IF !Isset! == TRUE (
        IF NOT EXIST "%KeyCnfFilePath%" (
          REM Teraterm環境設定ファイルのパスが見つからない場合のエラー処理
          ECHO [error] - "%KeyCnfFilePath%" is not found
          PAUSE
        ) ELSE (
          SET Option=!Option! /K=%KeyCnfFilePath%
        )
      )
      
      REM （背景色などの）環境設定ファイル指定がある場合、オプションを追加
      SET Isset=TRUE
      IF ""%IniFilePath%""=="""" SET Isset=FALSE
      IF "%IniFilePath%"=="""" SET Isset=FALSE
      IF !Isset! == TRUE (
        IF NOT EXIST "%IniFilePath%" (
          REM Teraterm環境設定ファイルのパスが見つからない場合のエラー処理
          ECHO [error] - "%IniFilePath%" is not found
          PAUSE
        ) ELSE (
          SET Option=!Option! /f=%IniFilePath%
        )
      )
      
      REM 接続
      START "" !TeratermPath! !Option!
      EXIT
    )
  )
)
ENDLOCAL